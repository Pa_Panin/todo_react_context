import React, { useContext } from "react";
import TodoItem from "./TodoItem";
import { MainContext } from "../Context";

const TodoList = () => {
  const { todos } = useContext(MainContext);
  return (
    <ul className="todo_list">
      {todos.map((todo) => {
        return (
          <TodoItem
            isDone={todo.check === "DONE"}
            key={todo.id}
            todo={todo}
            id={todo.id}
          />
        );
      })}
    </ul>
  );
};

export default TodoList;
