import React, { useContext } from "react";
import { MainContext } from "../Context";

const TodoItem = ({ todo, isDone }) => {
  const { clickTodo, removeTodo } = useContext(MainContext);
  const classDone = isDone ? "check" : "";
  return (
    <li className="el_list">
      <span className={classDone} onClick={() => clickTodo(todo.id)}>
        {todo.task}
      </span>
      <button className="removeTodoBtn" onClick={() => removeTodo(todo.id)} />
    </li>
  );
};

export default TodoItem;
