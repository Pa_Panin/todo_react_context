import React from "react";

const TodoCreateForm = ({ onCreate }) => {
  const [value, setValue] = React.useState("");

  const submitNewTodo = (event) => {
    event.preventDefault();

    if (value.trim()) {
      onCreate(value);
      setValue("");
    }
  };

  return (
    <form onSubmit={submitNewTodo}>
      <input
        placeholder="Новое дело"
        className="input_todo"
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <button className="edit_new_todo" type="submit">
        Добавить
      </button>
    </form>
  );
};

export default TodoCreateForm;
