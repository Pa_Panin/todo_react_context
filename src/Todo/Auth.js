import React from "react";

const Auth = ({ correctValidation }) => {
  const [signInToken, setSignInToken] = React.useState(true);
  const [authData, setAuthData] = React.useState({
    email: "",
    password: "",
  });

  const signInCheckKey = () => {
    if (signInToken) {
      setSignInToken(false);
    } else {
      correctValidation(authData, signInToken);
    }
  };

  const signUpCheckKey = () => {
    if (signInToken) {
      correctValidation(authData, signInToken);
    } else setSignInToken(true);
  };

  return (
    <div className="AuthForm">
      <div className="AuthForm">
        <div>
          {signInToken ? (
            <h1> Зарегистрируйтесь что бы войти в систему </h1>
          ) : (
            <h1>Введите логин и пароль</h1>
          )}
        </div>
        <div className="regContain">
          <input
            placeholder="Придумайте логин"
            value={authData.email}
            className="regInput"
            onChange={(e) =>
              setAuthData({ ...authData, email: e.target.value })
            }
          />
          <input
            placeholder="Придумайте пароль"
            value={authData.password}
            className="regInput"
            onChange={(e) =>
              setAuthData({ ...authData, password: e.target.value })
            }
          />
          <div>
            <button className="signInBtn" onClick={signInCheckKey}>
              Войти
            </button>
            <button className="signUpBtn" onClick={signUpCheckKey}>
              Зарегистрироваться
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Auth;
