import "./App.css";
import React, { useEffect, useContext } from "react";
import TodoList from "./Todo/TodoList.js";
import TodoCreateForm from "./Todo/TodoCreateForm.js";
import Auth from "./Todo/Auth.js";
import { Request } from "./query/Fetch.js";
import { MainContext } from "./Context.js";

const App = () => {
  const token = localStorage.getItem("token");
  const {
    addTodo,
    todos,
    handleClickOut,
    mapTodos,
    signInFunc,
    setAuthFlag,
    authFlag,
  } = useContext(MainContext);

  useEffect(() => {
    if (token) {
      setAuthFlag(true);
      const instanceGet = new Request();
      instanceGet
        .queryGET("http://localhost:3005/todo/list")
        .then((res) => {
          mapTodos(res);
        })
        .catch((res) => {
          alert(`Ошибка! Код ошибки ${res.statusCode}`);
        });
    } else setAuthFlag(false);
  }, [token]);

  return (
    <div className="container">
      {authFlag ? (
        <div>
          <div className="outContain">
            <div className="outHeader">
              <button
                type="button"
                className="outUser"
                onClick={handleClickOut}
              >
                Выйти из системы
              </button>
            </div>

            <h1 className="titleTodo">Ваш список дел</h1>
          </div>
          <div>
            <TodoCreateForm onCreate={addTodo} />
            <div>
              {todos.length ? <TodoList /> : <p> Ваш список дел пуст</p>}
            </div>
          </div>
        </div>
      ) : (
        <Auth correctValidation={signInFunc} />
      )}
    </div>
  );
};

export default App;
