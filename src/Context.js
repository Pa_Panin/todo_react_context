import React, { useState } from "react";
import { Request } from "./query/Fetch.js";
const SIGN_IN = "sign-in";
const SIGN_UP = "sign-up";

export const MainContext = React.createContext({
  addTodo: () => {},
  todos: [],
  removeTodo: () => {},
  clickTodo: () => {},
  handleClickOut: () => {},
  mapTodos: () => {},
  signInFunc: () => {},
  setAuthFlag: () => {},
  authFlag: [],
});

export const MainFunc = ({ children }) => {
  const [todos, setTodos] = useState([]);
  let [authFlag, setAuthFlag] = useState();

  const clickTodo = (id) => {
    const instancePatch = new Request();
    const PENDING = "PENDING";
    const DONE = "DONE";
    const todo = todos.find((el) => el.id === id);
    const patchTodo = {
      body: todo.task,
      status: todo.check === PENDING ? DONE : PENDING,
    };
    instancePatch
      .queryPATCH(`http://localhost:3005/todo/${id}`, patchTodo)
      .then((res) => {
        setTodos(
          todos.map((el) => {
            if (el.id === id) {
              el.check = res.status;
            }
            return el;
          })
        );
      })
      .catch((res) => {
        alert(`Ошибка! Код ошибки ${res.statusCode}`);
      });
  };

  const removeTodo = (id) => {
    const instanceDelete = new Request();
    instanceDelete
      .queryDELETE(`http://localhost:3005/todo/${id}`)
      .then(() => {
        setTodos(todos.filter((todo) => todo.id !== id));
      })
      .catch((res) => {
        alert(`Ошибка! Код ошибки ${res.statusCode}`);
      });
  };

  const handleClickOut = () => {
    localStorage.clear();
    setTodos([]);
  };

  const addTodo = (task) => {
    const instancePost = new Request();
    let body = {
      body: task,
    };

    instancePost
      .queryPOST("http://localhost:3005/todo", body)
      .then((res) => {
        setTodos([
          ...todos,
          {
            task: res.body,
            id: res.id,
            check: res.status,
          },
        ]);
      })
      .catch((res) => {
        alert(`Ошибка! Код ошибки ${res.statusCode}`);
      });
  };

  const mapTodos = (res) => {
    setTodos(
      res.map((temp) => ({
        task: temp.body,
        id: temp.id,
        check: temp.status,
      }))
    );
  };

  const signInFunc = (user, signInToken) => {
    const instancePost = new Request();
    let signKey = SIGN_UP;
    if (signInToken) {
      signKey = SIGN_UP;
    } else signKey = SIGN_IN;
    instancePost
      .queryPOST(`http://localhost:3005/auth/${signKey}`, user)
      .then((res) => {
        if (res.status === "ACTIVE") {
          localStorage.setItem("token", res.token);
          setAuthFlag(true);
        } else {
          alert("Введённые данные не корректны");
          setAuthFlag(false);
        }
        return;
      })
      .catch((res) => {
        alert(`Ошибка! Код ошибки ${res.statusCode}`);
      });
  };

  return (
    <MainContext.Provider
      value={{
        addTodo,
        todos,
        removeTodo,
        clickTodo,
        handleClickOut,
        mapTodos,
        signInFunc,
        setAuthFlag,
        authFlag,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};
