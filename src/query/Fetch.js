import Methods from "./Methods.js";

export class Request {
  checkLocalToken() {
    let userId;
    const localStorageToken = localStorage.getItem("token");
    if (localStorageToken) {
      userId = {
        authorization: localStorageToken,
      };
    }
    return userId;
  }

  query(method, url, body) {
    const newData = JSON.stringify(body);
    return fetch(url, {
      method: method,
      headers: {
        "Content-type": "application/json",
        ...this.checkLocalToken(),
      },
      body: newData,
    }).then((res) => res.json());
  }

  queryGET(url) {
    return this.query(Methods.GET, url, undefined);
  }
  queryPOST(url, body) {
    return this.query(Methods.POST, url, body);
  }
  queryPATCH(url, body) {
    return this.query(Methods.PATCH, url, body);
  }
  queryDELETE(url) {
    return this.query(Methods.DELETE, url, undefined);
  }
}
